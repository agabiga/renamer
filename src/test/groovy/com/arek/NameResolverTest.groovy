package com.arek

import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

/**
 *
 * Date: 05/04/17
 * Time: 21:57
 *
 * @author Arkadiusz Gabiga
 */
class NameResolverTest extends Specification {
    @Shared
    def tmp = "/tmp"

    @Unroll("should change name #name to #expected")
    def "should change name properly"() {
        when:
        def newName = NameResolver.resolveName(parent, name)

        then:
        newName == expected

        where:
        parent | name                     | expected
        null   | "test1.txt"              | "/test1.txt"
        tmp    | "test2.txt"              | "/tmp/test2.txt"
        tmp    | "ążśźęćńół.txt"          | "/tmp/azszecnol.txt"
        tmp    | "ążśźęćńółĄŻŚŹĘĆŃÓŁ.txt" | "/tmp/azszecnolAZSZECNOL.txt"
        tmp    | "abćĄśŁ %(ól.jpg"        | "/tmp/abcAsL___ol.jpg"

    }
}
