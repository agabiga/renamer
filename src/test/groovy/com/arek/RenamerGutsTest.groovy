package com.arek

import java.nio.file.Files
import java.nio.file.Paths

import spock.lang.Specification
import spock.lang.Unroll

/**
 *
 * Date: 08/04/17
 * Time: 22:23
 *
 * @author Arkadiusz Gabiga
 */
class RenamerGutsTest extends Specification {
    FileProvider fileProvider = Mock(FileProvider)

    RenamerGuts guts = new RenamerGuts(
            fileProvider: fileProvider
    )

    @Unroll("Should get #comment")
    def "Should retrieve location"() {
        given:
        def uri = new URI("file:/hidden/location.txt")

        File location = Mock(File)
        1 * location.exists() >> true
        1 * location.toURI() >> uri

        1 * fileProvider.getLocationFile() >> location
        1 * fileProvider.readLines(uri) >> lines
        0 * _

        when:
        def result = guts.getSavedLocation()

        then:
        result == expected

        where:
        lines                        | expected               | comment
        ["/hidden/location.txt"]     | "/hidden/location.txt" | "location from file"
        []                           | null                   | "null when empty location file"
        ["", "/hidden/location.txt"] | null                   | "null when first line is empty in location file"
    }

    def "Should return null when location file does not exist "() {
        given:
        File location = Mock(File)
        1 * location.exists() >> false
        1 * fileProvider.getLocationFile() >> location
        0 * _

        when:
        def result = guts.getSavedLocation()

        then:
        result == null
    }

    def "should process single file"() {
        given:
        def path = "/ąć.pdf"
        def newFile = new File(".")
        File file = fileMock("/", "ąć.pdf")
        1 * file.exists() >> true
        1 * file.renameTo(newFile)

        1 * fileProvider.newFile(path) >> file
        1 * fileProvider.newFile(_) >> newFile

        when:
        def result = guts.process(path)

        then:
        result == true
    }

    def "should process directory"() {
        given:
        def path = "/path/to/dir/"
        def newFile1 = new File("./ac.pdf")
        def newFile2 = new File("./lo.txt")

        File file1 = fileMock(path, "ąć.pdf")
        1 * file1.renameTo(newFile1)

        File file2 = fileMock(path, "łó.txt")
        1 * file2.renameTo(newFile2)

        File dir = Mock(File)
        1 * dir.exists() >> true
        1 * dir.isDirectory() >> true
        1 * dir.listFiles() >> [file1, file2]

        1 * fileProvider.newFile(path) >> dir
        1 * fileProvider.newFile(_) >> newFile1
        1 * fileProvider.newFile(_) >> newFile2

        when:
        def result = guts.process(path)

        then:
        result == true
    }

    def "should save location in file"() {
        given:
        def location = "/some/location"
        def file = new File(System.getProperty("java.io.tmpdir") + "/location.txt")
        1 * fileProvider.getLocationFile() >> file;

        when:
        guts.saveLocationInFile(location)

        then:
        Files.readAllLines(Paths.get(file.toURI()))[0] == location
        file.delete()
    }

    private File fileMock(String path, String filename) {
        File file = Mock(File)
        1 * file.isDirectory() >> false
        1 * file.isFile() >> true
        1 * file.getParent() >> path
        1 * file.getName() >> filename
        2 * file.getAbsolutePath() >> path + filename
        file
    }

}
