package com.arek;

import javafx.scene.control.Alert;

/**
 * Date: 07/04/17
 * Time: 23:11
 *
 * @author Arkadiusz Gabiga
 */
public class Alerts {
    public static void alert(String title, String msg) {
        log(title, msg);
        dialog(title, msg, Alert.AlertType.ERROR);
    }

    public static void info(String title, String msg) {
        log(title, msg);
        dialog(title, msg, Alert.AlertType.INFORMATION);
    }

    public static void alertSilent(String title, String msg) {
        System.out.println(title + ": " + msg);
    }

    private static void log(String title, String msg) {
        System.out.println(title + ": " + msg);
    }

    private static void dialog(String title, String msg, Alert.AlertType error) {
        Alert alert = new Alert(error);
        alert.setTitle(title);
        alert.setHeaderText(title);
        alert.setContentText(msg);
        alert.showAndWait();
    }

}
