package com.arek;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Date: 05/04/17
 * Time: 21:37
 *
 * @author Arkadiusz Gabiga
 */
public class NameResolver {
    private static Map<Character, Character> letters = new HashMap<>();

    static {
        letters.put('ą', 'a');
        letters.put('ś', 's');
        letters.put('ż', 'z');
        letters.put('ź', 'z');
        letters.put('ć', 'c');
        letters.put('ę', 'e');
        letters.put('ń', 'n');
        letters.put('ó', 'o');
        letters.put('ł', 'l');
        letters.put('Ą', 'A');
        letters.put('Ś', 'S');
        letters.put('Ż', 'Z');
        letters.put('Ź', 'Z');
        letters.put('Ć', 'C');
        letters.put('Ę', 'E');
        letters.put('Ń', 'N');
        letters.put('Ó', 'O');
        letters.put('Ł', 'L');
    }

    public static String resolveName(String parent, String name) {
        List<String> newName = new ArrayList<>();
        for(char c : name.toCharArray()) {
            if(!letters.containsKey(c)) {
                newName.add(Character.toString(c));
            } else {
                newName.add(Character.toString(letters.get(c)));
            }
        }

        int i = name.lastIndexOf('.');

        String filename = newName.stream().collect(Collectors.joining()).replaceAll("[^A-Za-z0-9]", "_");
        if(i > 0) {
            char[] chars = filename.toCharArray();
            chars[i] = '.';
            filename = String.valueOf(chars);
        }

        return emptyIfNull(parent) + "/" + filename;
    }

    private static String emptyIfNull(String str) {
        if(str == null) {
            return "";
        }
        return str;
    }
}
