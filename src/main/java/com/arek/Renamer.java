package com.arek;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

/**
 * Date: 03/04/17
 * Time: 22:10
 *
 * @author Arkadiusz Gabiga
 */
public class Renamer extends Application {

    RenamerGuts guts = new RenamerGuts();

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Text pathLbl = new Text("Ścieżka");
        TextField pathField = pathField();

        Button open = openButton(stage, pathField);
        Button start = startButton(pathField);

        VBox vBox = new VBox(5);
        vBox.getChildren().addAll(open, start);

        GridPane gridPane = gridPane();
        gridPane.add(pathLbl, 0, 0, 1, 1);
        gridPane.add(pathField, 1, 0, 1, 1);
        gridPane.add(vBox, 3, 0, 1, 2);

        Scene scene = scene(stage, gridPane);

        stageSetup(stage, scene);
    }

    private void stageSetup(Stage stage, Scene scene) {
        String title = title();

        stage.setResizable(false);
        stage.setTitle(title);
        stage.setScene(scene);
        stage.show();
    }

    private String title() {
        InputStream inputStream = this.getClass().getResourceAsStream("/config.properties");
        Properties properties = new Properties();
        try {
            properties.load(inputStream);
            String version = properties.getProperty("application.version", "");
            if(version != null && !version.isEmpty()) {
                return "Renamer " + version;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "Renamer";
    }

    private Scene scene(Stage stage, GridPane gridPane) {
        Scene scene = new Scene(gridPane);
        scene.addEventHandler(KeyEvent.KEY_PRESSED, keyEvent -> {
            if (keyEvent.getCode() == KeyCode.ESCAPE) {
                stage.close();
            }
        });
        return scene;
    }

    private GridPane gridPane() {
        GridPane gridPane = new GridPane();
        gridPane.setMinSize(500, 150);
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setVgap(5);
        gridPane.setHgap(5);
        gridPane.setAlignment(Pos.CENTER);
        return gridPane;
    }

    private Button startButton(TextField pathField) {
        Button start = new Button("Start");
        start.setMaxWidth(Double.MAX_VALUE);
        start.setOnAction(event -> {
            process(pathField.getText());
        });
        return start;
    }

    private Button openButton(Stage stage, TextField pathField) {
        Button open = new Button("Wybierz");
        open.setMaxWidth(Double.MAX_VALUE);
        open.setOnAction(event -> {
            DirectoryChooser chooser = new DirectoryChooser();
            File file = chooser.showDialog(stage);

            if (file != null) {
                pathField.setText(file.getAbsolutePath());
            }
        });
        return open;
    }

    private TextField pathField() {
        TextField pathField = new TextField();
        pathField.setMinWidth(300);
        pathField.setText(guts.getSavedLocation());
        pathField.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                process(pathField.getText());
            }
        });
        return pathField;
    }

    private void process(String path) {
        if (path != null) {
            if (guts.process(path)) {
                guts.saveLocationInFile(path);
                Alerts.info("Koniec", "Operacja zakończona");
            } else {
                Alerts.alert("Błąd", "Podana ścieżka nie istnieje.");
            }
        } else {
            Alerts.alert("Błąd", "Nie wybrano ścieżki");
        }
    }


}
