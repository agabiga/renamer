package com.arek;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Date: 07/04/17
 * Time: 23:11
 *
 * @author Arkadiusz Gabiga
 */
public class RenamerGuts {

    FileProvider fileProvider = new FileProvider();

    public String getSavedLocation() {
        File location = fileProvider.getLocationFile();
        if (location.exists()) {
            try {
                List<String> lines = fileProvider.readLines(location.toURI());
                if (lines.isEmpty()) {
                    return null;
                }

                String destination = lines.get(0);
                if (destination.isEmpty()) {
                    Alerts.alertSilent("Błąd", "Proszę wpisać ścieżke do katalogu w pierwszej linii pliku location.txt");
                    return null;
                }
                return destination;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        return null;
    }

    public boolean process(String dest) {
        File file = fileProvider.newFile(dest);
        if (!file.exists()) {
            return false;
        }

        rename(file);
        return true;
    }

    public void saveLocationInFile(String dest) {
        File location = fileProvider.getLocationFile();

        try (FileWriter fw = new FileWriter(location)) {
            fw.write(dest);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void rename(File file) {
        if (file.isDirectory()) {
            Arrays.stream(emptyIfNull(file.listFiles())).forEach(f -> rename(f));
        } else if (file.isFile()) {
            File newFile = fileProvider.newFile(NameResolver.resolveName(file.getParent(), file.getName()));

            if (file.getAbsolutePath().equals(newFile.getAbsolutePath())) {
                System.out.println("Nu Nu Nu! Ten plik jest ok: " + file.getAbsolutePath());
            } else {
                file.renameTo(newFile);
                System.out.println(file.getAbsolutePath() + " ---> " + newFile.getAbsolutePath());
            }
        }
    }

    private File[] emptyIfNull(File[] files) {

        return files != null ? files : new File[0];
    }


}
