package com.arek;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * Date: 08/04/17
 * Time: 22:03
 *
 * @author Arkadiusz Gabiga
 */
public class FileProvider {
    public static final String RENAME_JAR = "rename.jar";

    public File getLocationFile() {
        String cwd = FileProvider.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        if (cwd.endsWith(RENAME_JAR)) {
            cwd = cwd.substring(0, cwd.length() - RENAME_JAR.length());
        }

        return new File(cwd + "location.txt");
    }

    public List<String> readLines(URI uri) throws IOException {
        return Files.readAllLines(Paths.get(uri));
    }

    public File newFile(String path) {
        return new File(path);
    }
}
